.PHONY: all build genkey run copykey setupgitolite attach stop attachroot clone

all: build run genkey copykey setupgitolite

# build the docker image from the Dockerfile
build:
	docker build -t flash4syth/git-server -f Dockerfile .

# Start container in background letting the default CMD run sshd
# and mount local dir to persist repositories to disk
run:
	docker compose up -d

# generate admin/admin.pub private/public key pair with
# no passphrase
genkey:
	if [ ! -e "admin.pub" ]; then \
		ssh-keygen -q -f admin -N ''; \
	fi

# Copy previously generated public key (generated with `ssh-keygen`)
copykey:
	docker cp admin.pub git-server-git-server-1:/home/git/admin.pub

# setup gitolite with the admin.pub key
setupgitolite:
	docker exec -u git git-server-git-server-1 gitolite setup -pk admin.pub

# attach to container as git user
attach:
	docker exec -it -u git git-server-git-server-1 /bin/sh

# attach to container as admin user
attachroot:
	docker exec -it -u root git-server-git-server-1 /bin/sh

stop:
	docker compose down

# clone git repository in the container
clone:
	git clone ssh://git@`docker inspect \
	  -f '{{range.NetworkSettings.Networks}}{{.IPAddress}}{{end}}' \
	  git-server`:22/gitolite-admin

# ssh into container
ssh:
	ssh -i admin git@`docker inspect \
	  -f '{{range.NetworkSettings.Networks}}{{.IPAddress}}{{end}}' \
	  git-server`
