FROM alpine:3.21.0

# Install required dependencies
RUN apk --no-cache add \
    openssh \
    git \
    perl \
    perl-utils

# Create a user for Gitolite
RUN adduser -D -s /bin/sh git && \
    echo "git:*" | chpasswd -e # this line was needed to unlock user

# Generate SSH host keys otherwise /usr/sbin/sshd will fail to start
# Host keys should not have passphrases
RUN ssh-keygen -A

# Configure sshd daemon
RUN sed -i 's/#PasswordAuthentication yes/PasswordAuthentication no/' /etc/ssh/sshd_config

# Switch to the git user
USER git

# Add gitolite binary to PATH for build-time of this image
ENV PATH="/home/git/bin:${PATH}"

# ensure PATH is as above during runtime for the git user
RUN echo 'export PATH="/home/git/bin:${PATH}"' >> /home/git/.profile

# Set up Gitolite
WORKDIR /home/git
RUN git clone --branch master --single-branch https://github.com/jon4syth/gitolite
RUN mkdir -p bin
RUN gitolite/install -ln /home/git/bin

# Expose SSH port
EXPOSE 22

# Start the SSH daemon - Must be root to start this
USER root
CMD ["/usr/sbin/sshd", "-D", "-e"]

